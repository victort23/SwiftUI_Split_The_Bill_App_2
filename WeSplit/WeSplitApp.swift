//
//  WeSplitApp.swift
//  WeSplit
//
//  Created by Vancea Tapalaga Victor on 10.01.2023.
//


// Responsable to configure the initial launch of our program
// Any data I want to create and have a live perspective I put in here
import SwiftUI

@main
struct WeSplitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
