//
//  ContentView.swift
//  WeSplit
//
//  Created by Vancea Tapalaga Victor on 23.01.2023.
//

import SwiftUI

struct ContentView: View {
    @State private var checkAmount = 0.0
    @State private var nrOfPers = 2
    @State private var tipPercentage = 20
    @FocusState private var amountIsFocused : Bool
    //@State whenerver something of that State changes it reload the body to actualize the state
    //These views used to create the UI do the right thing based on the platform I progrm the app to
    let tipProcentages = [10,15,20,25,0]
    var type : FloatingPointFormatStyle<Double>.Currency = .currency(code: Locale.current.currencyCode ?? "EUR")
    var totalPerPerson : Double{ // computed property
        //calculate total per person
        let pplCount = Double(nrOfPers+2)
        let tipSelection = Double(tipPercentage)
        let tipValue = checkAmount / 100 * tipSelection
        let grandTotal = checkAmount + tipValue
        let amountPerPerson = grandTotal / pplCount
        
        
        return amountPerPerson
    }
    
    var body: some View {
        NavigationView{ // let ios slide in new view as needed
            Form{
                Section{
                    //Locale is a massive Struct built and incorpored by IOS libraries that find the user region setted from settings and find out it's local currency
                    TextField("Amount : ", value:$checkAmount ,format: .currency(code: Locale.current.currencyCode ?? "EUR") ).keyboardType(.decimalPad)
                        .focused($amountIsFocused) // it focuses on the TextField
                    //COMMAND + SHIFT + K to see switch between hardware keyboard or decimalPad
                    Picker("Number of people", selection: $nrOfPers){ // the 2 inside the nrOfPers selects the 2th elements inside picker , it being 4{
                        ForEach(2..<99){
                            Text("\($0)")
                            
                        }
                    }
                }
                NavigationLink(LocalizedStringKey("Percent of tip to leave:  \(tipPercentage)%")){ // it opens a new screen , is for prototipying, it
                   // Section{
                    Form{
                        Picker("Tip percentage", selection: $tipPercentage){
                            ForEach(0..<100){
                                Text($0,format: .percent)
                            }
                        }.pickerStyle(.inline)
                        
                        //  } header: { // it places above the section it's header title
                        //    Text("How much tips you'd like to leave ?")
                    }
                }
                Section{
                    Text(totalPerPerson,format: type)
                } header: {
                    Text("Amount per person")
                }
                Section{
                    let grand = totalPerPerson * Double(nrOfPers+2)
                    Text(grand,format: type)
                }header: {
                    Text("Total amount ")
                
                }
            }.navigationTitle("WeSplit") //should be attached to Form itself
                .toolbar{ // let us specify tool bars items for view , top or bottom as examples
                    ToolbarItemGroup(placement: .keyboard){ // here we place buttons on the toolbar , we specify in the placement that the buttons should be on the keyboard
                        Spacer() // Place the 'Done' button to the right side of the toolBar , depending on what we use
                        Button("Done"){
                            amountIsFocused = false
                        }
                    }
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
